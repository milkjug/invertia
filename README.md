### Invertia

Invertia is a simple night-mode solution switch for Google Chrome that changes a webpage to inverted colors for better reading at night

### Installing 
Simply download the extension

go to `chrome://extensions/` 

click on 'Developer Mode' and load the unpacked extension from the unzipped directory. 

Have fun reading at night!